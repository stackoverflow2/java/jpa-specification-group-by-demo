package com.example.jpaspecificationdemo.model;

import lombok.*;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemSearchParameter {

    private Optional<String> location = Optional.empty();

    private Optional<String> containerCode = Optional.empty();

}
