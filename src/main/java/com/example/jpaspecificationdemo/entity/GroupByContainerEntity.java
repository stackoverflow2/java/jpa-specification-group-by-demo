package com.example.jpaspecificationdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "item")
public class GroupByContainerEntity {

    @Id
    private String idItem;

    private String idContainer;

    private String idLocation;

    private String containerName;

    private String containerCode;

    @Formula("(GROUP_CONCAT(id_location))")
    private String locations;

    @Formula("(SUM(amount))")
    private Integer sumAmount;

    @Formula("(COUNT(container_code))")
    private Integer countItems;

}
