package com.example.jpaspecificationdemo.specification.item;

import com.example.jpaspecificationdemo.entity.GroupByContainerEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

@RequiredArgsConstructor
public class LocationSpecification implements Specification<GroupByContainerEntity> {

    private final String location;

    /**
     * Search by location
     * @param root must not be {@literal null}.
     * @param query must not be {@literal null}.
     * @param criteriaBuilder must not be {@literal null}.
     * @return
     */
    @Override
    public Predicate toPredicate(Root<GroupByContainerEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        return criteriaBuilder.equal(root.get("idLocation"), location);
    }
}
