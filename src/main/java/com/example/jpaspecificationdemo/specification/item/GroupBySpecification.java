package com.example.jpaspecificationdemo.specification.item;

import com.example.jpaspecificationdemo.entity.GroupByContainerEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class GroupBySpecification implements Specification<GroupByContainerEntity> {

    @Override
    public Predicate toPredicate(Root<GroupByContainerEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        query.groupBy(root.get("containerCode"));
        return criteriaBuilder.equal(criteriaBuilder.literal(1), criteriaBuilder.literal(1));
    }

}
