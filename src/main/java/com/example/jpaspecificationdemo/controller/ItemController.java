package com.example.jpaspecificationdemo.controller;

import com.example.jpaspecificationdemo.entity.GroupByContainerEntity;
import com.example.jpaspecificationdemo.model.ItemSearchParameter;
import com.example.jpaspecificationdemo.service.GroupByContainerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ItemController {

    private final GroupByContainerService service;

    @GetMapping("/search")
    public Page<GroupByContainerEntity> search(ItemSearchParameter parameter, Pageable pageable) {
        return service.findAll(parameter, pageable);
    }

}
