package com.example.jpaspecificationdemo.service;

import com.example.jpaspecificationdemo.entity.GroupByContainerEntity;
import com.example.jpaspecificationdemo.model.ItemSearchParameter;
import com.example.jpaspecificationdemo.repository.IGroupByContainerRepository;
import com.example.jpaspecificationdemo.specification.item.ContainerCodeSpecification;
import com.example.jpaspecificationdemo.specification.item.GroupBySpecification;
import com.example.jpaspecificationdemo.specification.item.LocationSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;


@Service
@RequiredArgsConstructor
public class GroupByContainerService {

    private final IGroupByContainerRepository repository;

    public Page<GroupByContainerEntity> findAll(ItemSearchParameter params, Pageable pageable) {

        List<Pair<Optional<?>, Function<Object, Specification<GroupByContainerEntity>>>> pairs = List.of(
                Pair.of(params.getLocation(), s -> new LocationSpecification((String) s)),
                Pair.of(params.getContainerCode(), c -> new ContainerCodeSpecification((String) c))
        );

        Specification<GroupByContainerEntity> spec = pairs.stream()
                .filter(entry -> entry.getFirst().isPresent())
                .map(entry -> entry.getSecond().apply(entry.getFirst().get()))
                .reduce(new GroupBySpecification(), Specification::and);

        return repository.findAll(spec, pageable);
    }

}
